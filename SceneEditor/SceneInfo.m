//
//  SceneInfo.m
//  SceneEditor
//
//  Created by lh on 14-1-9.
//  Copyright (c) 2014年 lh. All rights reserved.
//  www.9miao.com

#import "SceneInfo.h"

@implementation SceneInfo


@synthesize enemyArr;
@synthesize resourceName;
@synthesize right;
@synthesize left;
@synthesize monsterId;
@synthesize up;
@synthesize down;
@synthesize nextSceneId;
@synthesize sceneType;
@synthesize sceneId;
@synthesize sceneName;
//@synthesize releaseRole;


@end
