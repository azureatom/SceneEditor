//
//  Person.h
//  SceneEditor
//
//  Created by lh on 14-1-9.
//  Copyright (c) 2014年 lh. All rights reserved.
//  www.9miao.com

#import <Foundation/Foundation.h>
#import "SBJson.h"
@interface Person : NSObject
    

@property (retain,nonatomic)NSMutableDictionary *properties;
@property (retain,nonatomic)NSMutableDictionary *staticProperties;
@property (retain,nonatomic)NSString *skill_Id;
@property (retain,nonatomic)NSNumber *res_icon;
@property (retain,nonatomic)NSNumber *res_resource;
@property (retain,nonatomic)NSString *color;
@property (retain,nonatomic)NSString *btype;
@property (retain,nonatomic)NSString *coatt;
@property (retain,nonatomic)NSNumber *dou;
@property (retain,nonatomic)NSNumber *ske;
@property (retain,nonatomic)NSNumber *m_level;//等级
@property (retain,nonatomic)NSNumber *m_id;// id
@property (retain,nonatomic)NSNumber *m_petid;//模板id
@property (retain,nonatomic)NSString *m_name;//名字

@property (retain,nonatomic)NSNumber * m_attack;//攻击
@property (retain,nonatomic)NSNumber * m_hp;//血量
@property (retain,nonatomic)NSNumber * m_defense;//防御
//隐藏属性
@property (retain,nonatomic)NSNumber * m_hittarget;//命中
@property (retain,nonatomic)NSNumber * m_tenacity;//韧性
@property (retain,nonatomic)NSNumber * m_dodge;//闪避
@property (retain,nonatomic)NSNumber * m_parry;//格挡
@property (retain,nonatomic)NSNumber * m_strike;//暴击
@property (retain,nonatomic)NSNumber * m_strike_result;//暴击威力
//其他属性
@property (retain,nonatomic)NSNumber * m_range;//攻击范围
@property (retain,nonatomic)NSNumber * m_speed;//移动速度
@property (retain,nonatomic)NSNumber * m_attack_speed;//攻击速度
//固定属性
@property (retain,nonatomic)NSNumber * base_hp;//标准生命
@property (retain,nonatomic)NSNumber * base_attack;//标准攻击
@property (retain,nonatomic)NSNumber * base_strike;//标准暴击值
@property (retain,nonatomic)NSNumber * defense_cf;//防御系数
@property (retain,nonatomic)NSNumber * dodge_cf;//闪避系数
@property (retain,nonatomic)NSNumber * tenacity_cf;//韧性系数
@property (retain,nonatomic)NSNumber * parry_cf;//格挡系数
@property (retain,nonatomic)NSNumber * deflect_cf;//偏斜系数
@property (retain,nonatomic)NSNumber * strike_cf;//暴击率系数
@property (retain,nonatomic)NSNumber * strike_result_cf;//暴击威力系数
@property (retain,nonatomic)NSNumber * hitrate_cf;//命中系数

@property (retain,nonatomic)NSNumber * damage_rate;//伤害率
@property (retain,nonatomic)NSNumber * avoid_rate;//躲避率
@property (retain,nonatomic)NSNumber * parry_rate;//格挡率
@property (retain,nonatomic)NSNumber * strike_rate;//暴击率
@property (retain,nonatomic)NSString *skillId;//技能id
@property (assign) BOOL isSave;
-(void)setProperties;
-(void)setStaticProperties;
@end
